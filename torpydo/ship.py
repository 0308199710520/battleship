from enum import Enum
import re
class Color(Enum):
    CADET_BLUE = 1
    CHARTREUSE = 2
    ORANGE = 3
    RED = 4
    YELLOW = 5

class Letter(Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5
    F = 6
    G = 7
    H = 8

class Position(object):
    def __init__(self, column: Letter, row: int):
        self.column = column
        self.row = row

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __str__(self):
        return f"{self.column.name}{self.row}"

    __repr__ = __str__

class Ship(object):
    def __init__(self, name: str, size: int, color: Color):
        self.name = name
        self.size = size
        self.color = color
        self.positions = []


    def add_position(self, _input: str, alignment:str):
        letter = Letter[_input.upper()[:1]]
        number = int(_input[1:])
        letter_array = [Letter.A, Letter.B, Letter.C, Letter.D, Letter.E, Letter.F, Letter.G, Letter.H]
        x = letter_array.index(letter)
        #position = Position(letter, number)
        if alignment.lower() == "h":
            for i in range(self.size):
                self.positions.append(Position(letter, number+i))
        else:
            for i in range(self.size):
                self.positions.append(Position(letter_array[x+i], number))

    def __str__(self):
        return f"{self.color.name} {self.name} ({self.size}): {self.positions}"

    __repr__ = __str__
