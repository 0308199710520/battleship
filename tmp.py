import string
import re
from enum import Enum


class Letter(Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5
    F = 6
    G = 7
    H = 8

class Position(object):
    def __init__(self, column: Letter, row: int):
        self.column = column
        self.row = row

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __str__(self):
        return f"{self.column.name}{self.row}"

    __repr__ = __str__

# def input_validator(func):
#     def inner():
#         return func()
#     return inner


def input_validator(func):
    def inner(_input):
        if re.match(r'[a-hA-H][1-8]', str(_input)):
            return func(_input)
        _new_input = input("Invalid position, please enter a valid position")
        return inner(_new_input)
    return inner

@input_validator
def parse_position(_input: str):
    letter = Letter[_input.upper()[:1]]
    number = int(_input[1:])
    position = Position(letter, number)

    return Position(letter, number)

print(parse_position("A9"))
